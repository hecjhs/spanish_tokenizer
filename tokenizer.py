 # -*- coding: utf-8 -*-

'''
Version 1.1
Created on 23/03/2016, tested for Python 2.7

'''

import codecs, os, string, re, nltk
from collections import Counter


class splitcorpus(object):
    """docstring for splitcorpus"""
    def __init__(self, corpus_path):
        super(splitcorpus, self).__init__()
        self.corpus_path = corpus_path

    def parserfile_by_chars(self):
        '''
            Elimina los signos de puntuación dejando solo las palabras
        '''
        path = self.corpus_path
        print 'path', path
        ls_dir = os.walk(path)
        for root, dirs, files in ls_dir:
            for fichero in files:
                (nombreFichero, extension) = os.path.splitext(fichero)
                if(extension == '.split_punct'):
                    file_content1 = codecs.open(str(root) + os.sep + fichero, 'r', 'utf-8', 'replace').read()
                    newfile = codecs.open(str(root) + os.sep + fichero.replace('split_punct', 'split'), 'w', 'utf-8', 'replace')
                    content = []
                    for file_content in file_content1.split('\n'):
                        file_content = re.sub('([:.,\+\*#/$&~;<=>@^_`´{|}"¡!¿?()«»])', r'', file_content)
                        # Comillas dobles
                        # Unicode para “ u'\u201C'
                        file_content = re.sub(u'\u201C', r'', file_content)
                        # Unicode para ” u'\u201d'
                        file_content = re.sub(u'\u201d', r'', file_content)
                        # Unicode para ″
                        file_content = re.sub(u'\u2033', r'', file_content)
                        # Unicode para ‘ u'\u07F5'
                        file_content = re.sub(u'\u07F5', r'', file_content)
                        # Unicode para ’ u'\u07F4'
                        file_content = re.sub(u'\u07F4', r'', file_content)
                        #Unicode para ՚ 055A
                        file_content = re.sub(u'\u055A', r'', file_content)
                        #Unicode para ʻ
                        file_content = re.sub(u'\u02BB', r'', file_content)

                        # Unicode para – u'\u2013'
                        file_content = re.sub(u'\u2013', r'', file_content)
                        # Unicode para — u'\u2014'
                        file_content = re.sub(u'\u2014', r'', file_content)
                        # Unicode para …
                        file_content = re.sub(u'\u2026', r'', file_content)
                        # Unicode para  ’
                        file_content = re.sub(u'\u2019', r'', file_content)

                        file_content = re.sub(u'\u2018', r'', file_content)

                        file_content = re.sub('\'', r'', file_content)
                        file_content = re.sub('(\'s)', r' \1', file_content)
                        file_content = re.sub('-', ' ', file_content)
                        file_content = re.sub('%', '', file_content)
                        file_content = re.sub('\[', '', file_content)
                        file_content = re.sub('\]', '', file_content)
                        file_content = file_content.replace('\\', '')
                        file_content = re.sub('[^\S\r\n]{2,}', ' ', file_content)
                        file_content = re.sub('([:1234567890])', r'', file_content)
                        content.append(file_content.lower())
                    newfile.write('\n'.join(content))


    def parserfile_by_punct(self):
        '''
            Separa las palabras de los signos de puntuación dejando un espacio
            simple entre ellos.
        '''
        path = self.corpus_path
        ls_dir = os.walk(path)
        for root, dirs, files in ls_dir:
            for fichero in files:
                # print fichero
                (nombreFichero, extension) = os.path.splitext(fichero)
                if(extension == '.txt'):
                    file_content1 = codecs.open(str(root) + os.sep + fichero,'r', 'utf-8', 'replace').read()
                    newfile = codecs.open(str(root)+ os.sep + fichero[0:-4]+'.split_punct','w', 'utf-8', 'replace')
                    content = ''
                    for file_content in file_content1.split('\n'):
                        file_content = re.sub('([:.,\+\*#/$&~;<=>@^_`´{|}"¡!¿?()«»])', r' \1 ', file_content)
                        # Comillas dobles
                        # Unicode para “ u'\u201C'
                        file_content = re.sub(u'\u201C', u' \u201C ', file_content)
                        # Unicode para ” u'\u201d'
                        file_content = re.sub(u'\u201d', u' \u201d ', file_content)
                        # Unicode para ″
                        file_content = re.sub(u'\u2033', u' \u2033 ', file_content)
                        # Unicode para ‘ u'\u07F5'
                        file_content = re.sub(u'\u07F5', u' \u07F5 ', file_content)
                        # Unicode para ’ u'\u07F4'
                        file_content = re.sub(u'\u07F4', u' \u07F4 ', file_content)
                        #Unicode para ՚ 055A
                        file_content = re.sub(u'\u055A', u' \u055A ', file_content)
                        #Unicode para ʻ
                        file_content = re.sub(u'\u02BB', u' \u02BB ', file_content)

                        # Unicode para – u'\u2013'
                        file_content = re.sub(u'\u2013', u' \u2013 ', file_content)
                        # Unicode para — u'\u2014'
                        file_content = re.sub(u'\u2014', u' \u2014 ', file_content)
                        # Unicode para …
                        file_content = re.sub(u'\u2026', u' \u2026 ', file_content)
                        # Unicode para  ’
                        file_content = re.sub(u'\u2019', u' \u2019 ', file_content)

                        file_content = re.sub(u'\u2018', u' \u2018 ', file_content)


                        file_content = re.sub('(\'s)', r' \1', file_content)
                        file_content = re.sub('-', ' - ', file_content)
                        file_content = re.sub('%', ' % ', file_content)
                        file_content = re.sub('\[', ' [ ', file_content)
                        file_content = re.sub('\]', ' ] ', file_content)
                        file_content = file_content.replace('\\',' \ ')
                        file_content = re.sub('[^\S\r\n]{2,}', ' ', file_content)
                        # file_content = re.sub('([:1234567890])', r'', file_content)
                        content = content + file_content + '\n'
                    newfile.write(content.lower())
 
if __name__ == '__main__':
    # ruta para el corpus (archivos .txt)
    corpus_path = ''

    # seleccionar idioma para trabajar es -> español
    # seleccionar idioma para trabajar en -> ingles 
    
    splitcorpus(corpus_path, dictsyl).parserfile_by_punct()
    splitcorpus(corpus_path, dictsyl).parserfile_by_chars()

    print 'Done!!'