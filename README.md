# README #

TOKENIZER PARA EL IDIOMA ESPAÑOL
### Descripción ###

Alternativa a NLTK, para la tokenización de un corpus en español. 



### Configuración ###

Solo implementar el path en donde se encuentra el corpus (archivos .txt)

Se generaran archivos auxiliares .split y .split_punct 

### Archivos .split  ###

	Eliminan todos los signos de puntuación considera caracteres especiales y que no se emplean en el inglés. 

	Ejemplo:
	   Original: 
	   		﻿Artistas que usaron fluidos corporales para sus más grandes creaciones
			 
			 En una carrera salvaje por crear e innovar, el humano ha experimentado con todas las posibilidades que su cuerpo, mente y alma le permite con el fin de no quedarse inmóvil y ser devorado por el feroz lobo que llaman vida. Con tal de encontrar lo oculto e inimaginable, muchas personas se han atrevido a desafiar las reglas de la moral y la estética. Algunos críticos dicen que estas personas están locas, otros aseguran que sólo trabajan libremente con sus fetiches, por ejemplo, el de la urolagnia. 

		Salida:
			﻿artistas que usaron fluidos corporales para sus más grandes creaciones

	 		en una carrera salvaje por crear e innovar el humano ha experimentado con todas las posibilidades que su cuerpo mente y alma le permite con el fin de no quedarse inmóvil y ser devorado por el feroz lobo que llaman vida con tal de encontrar lo oculto e inimaginable muchas personas se han atrevido a desafiar las reglas de la moral y la estética algunos críticos dicen que estas personas están locas otros aseguran que sólo trabajan libremente con sus fetiches por ejemplo el de la urolagnia 




### Archivos .split_punct  ###

	Separa . 

		Ejemplo:
		   Original: 
		   		﻿Artistas que usaron fluidos corporales para sus más grandes creaciones
				 
				 En una carrera salvaje por crear e innovar, el humano ha experimentado con todas las posibilidades que su cuerpo, mente y alma le permite con el fin de no quedarse inmóvil y ser devorado por el feroz lobo que llaman vida. Con tal de encontrar lo oculto e inimaginable, muchas personas se han atrevido a desafiar las reglas de la moral y la estética. Algunos críticos dicen que estas personas están locas, otros aseguran que sólo trabajan libremente con sus fetiches, por ejemplo, el de la urolagnia. 

			Salida:
				artistas que usaron fluidos corporales para sus más grandes creaciones

 				en una carrera salvaje por crear e innovar , el humano ha experimentado con todas las posibilidades que su cuerpo , mente y alma le permite con el fin de no quedarse inmóvil y ser devorado por el feroz lobo que llaman vida . con tal de encontrar lo oculto e inimaginable , muchas personas se han atrevido a desafiar las reglas de la moral y la estética . algunos críticos dicen que estas personas están locas , otros aseguran que sólo trabajan libremente con sus fetiches , por ejemplo , el de la urolagnia .